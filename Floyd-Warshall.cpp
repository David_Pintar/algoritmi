﻿#include <iostream>
#include <fstream>

using namespace std;

int vel;

int C[30][30];
int D[30][30];
int pi[30][30];

void FLOYD_WARSHALL()
{
	for (int i = 0; i < vel; i++)
	{
		for (int j = 0; j < vel; j++)
		{
			D[i][j] = C[i][j];
			if (i != j && C[i][j] != 9999)
			{
				pi[i][j] = i;
			}
			else
				pi[i][j] = -1;
		}
	}
	for (int k = 0; k < vel; k++)
	{
		for (int i = 0; i < vel; i++)
		{
			for (int j = 0; j < vel; j++)
			{
				if (D[i][j] > D[i][k] + D[k][j])
				{
					D[i][j] = D[i][k] + D[k][j];
					pi[i][j] = pi[k][j];
				}
			}
		}
	}
}

void IZPIS_POTI(int s, int g)
{
	if (s == g)
	{
		cout << s << endl;
	}
	else
	{
		if (pi[s][g] == -1)
		{
			cout << "med " << s << " in " << g << " ni poti." << endl;
		}
		else
		{
			IZPIS_POTI(s, pi[s][g]);
			cout << " " << g;
		}
	}
	
}

void beri(string f)
{
	fstream dat(f.c_str(), fstream::in);
	dat >> vel;
	int i = 0;
	int p, q, c;

	for (int i = 0; i < vel; i++)
	{
		for (int j = 0; j < vel; j++)
		{
			if (i == j)
			{
				C[i][j] = 0;
			}
			else
			{
				C[i][j] = 9999;
			}
		}
	}

	while (!dat.eof())
	{
		dat >> p >> q >> c;
		C[p - 1][q - 1] = c;
		i++;
	}
	dat.close();
}

void meni()
{

	cout << "================================================" << endl;
	cout << "|| 1 - PREBERI GRAF IZ DATOTEKE               ||" << endl;
	cout << "|| 2 - ZAGON ALGORITMA                        ||" << endl;
	cout << "|| 3 - IZPIS NAJKRAJSE POTI MED s IN g        ||" << endl;
	cout << "|| 4 - KONEC                                  ||" << endl;
	cout << "================================================" << endl;
}

int main()
{
	int s;
	int g;

	int x;
	
	do
	{
		meni();
		cin >> x;
		switch (x)
		{
		case 1:
			beri("Graf.txt");
			cout << "GRAF PREBRAN" << endl;
			cout << "Veliksot grafa: " << vel << endl;
			break;

		case 2:
			FLOYD_WARSHALL();
			break;

		case 3:
			cout << "Vstavi s: ";
			cin >> s;
			cout << "Vstavi g: ";
			cin >> g;
			IZPIS_POTI(s, g);
			cout << endl;
			cout << "Cena je: " << D[s][g];
			cout << endl;
			break;

		default:
			cout << "Nepravilen vnos!" << endl;
		}

	} while (x != 4);

	return 0;
}
